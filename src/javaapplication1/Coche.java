/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Administrador
 */
public class Coche {
    
    private Motor motor;
    private Rueda ruedaDelanteraDerecha, ruedaDelanteraIzquierda, ruedaTraseraDerecha, ruedaTraseraIzquierda;
    private Puerta puertaDerecha, puertaIzquierda;
    
    private boolean encendido;

    public Coche() {
        this.motor = new Motor();
        this.ruedaDelanteraDerecha = new Rueda();
        this.ruedaDelanteraIzquierda = new Rueda();
        this.ruedaTraseraDerecha = new Rueda();
        this.ruedaTraseraIzquierda = new Rueda();
        this.puertaDerecha = new Puerta();
        this.puertaIzquierda = new Puerta();
        this.encendido = false;
    }

    public Coche(Motor motor, Rueda ruedaDelanteraDerecha, Rueda ruedaDelanteraIzquierda, Rueda ruedaTraseraDerecha, Rueda ruedaTraseraIzquierda, Puerta puertaIzquierda, boolean encendido) {
        this.motor = motor;
        this.ruedaDelanteraDerecha = ruedaDelanteraDerecha;
        this.ruedaDelanteraIzquierda = ruedaDelanteraIzquierda;
        this.ruedaTraseraDerecha = ruedaTraseraDerecha;
        this.ruedaTraseraIzquierda = ruedaTraseraIzquierda;
        this.puertaIzquierda = puertaIzquierda;
        this.encendido = encendido;
    }

    public Motor getMotor() {
        return this.motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    public Puerta getPuertaDerecha() {
        return this.puertaDerecha;
    }

    public void setPuertaDerecha(Puerta puertaDerecha) {
        this.puertaDerecha = puertaDerecha;
    }

    public Puerta getPuertaIzquierda() {
        return this.puertaIzquierda;
    }

    public void setPuertaIzquierda(Puerta puertaIzquierda) {
        this.puertaIzquierda = puertaIzquierda;
    }

    public Rueda getRuedaDelanteraDerecha() {
        return this.ruedaDelanteraDerecha;
    }

    public void setRuedaDelanteraDerecha(Rueda ruedaDelanteraDerecha) {
        this.ruedaDelanteraDerecha = ruedaDelanteraDerecha;
    }

    public Rueda getRuedaDelanteraIzquierda() {
        return this.ruedaDelanteraIzquierda;
    }

    public void setRuedaDelanteraIzquierda(Rueda ruedaDelanteraIzquierda) {
        this.ruedaDelanteraIzquierda = ruedaDelanteraIzquierda;
    }

    public Rueda getRuedaTraseraDerecha() {
        return this.ruedaTraseraDerecha;
    }

    public void setRuedaTraseraDerecha(Rueda ruedaTraseraDerecha) {
        this.ruedaTraseraDerecha = ruedaTraseraDerecha;
    }

    public Rueda getRuedaTraseraIzquierda() {
        return this.ruedaTraseraIzquierda;
    }

    public void setRuedaTraseraIzquierda(Rueda ruedaTraseraIzquierda) {
        this.ruedaTraseraIzquierda = ruedaTraseraIzquierda;
    }

    public boolean isEncendido() {
        return this.encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }
    
    public void prender() {
        this.encendido = true;
        this.motor.setPrendido(true);
    }
    
    public void inflarRuedaDerechaDelantera() {
        this.ruedaDelanteraDerecha.setInflada(true);
    }
    
    public void inflarRuedaIzquierdaDelantera() {
        this.ruedaDelanteraIzquierda.setInflada(true);
    }
    
    public void inflarRuedaDerechaTrasera() {
        this.ruedaTraseraDerecha.setInflada(true);
    }
    
    public void inflarRuedaIzquierdaTrasera() {
        this.ruedaTraseraIzquierda.setInflada(true);
    }
    
    public void abrirPuertaDerecha() {
        this.puertaDerecha.setAbierta(true);
    }
    
    public void abrirPuertaIzquierda() {
        this.puertaIzquierda.setAbierta(true);
    }
    
    public void abrirVentanaPuertaDerecha() {
        this.puertaDerecha.getVentana().setAbierta(true);
    }
    
    public void abrirVentaPuertaIzquierda() {
        this.puertaIzquierda.getVentana().setAbierta(true);
    }
    
    
}
