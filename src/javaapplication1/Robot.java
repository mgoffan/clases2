/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Administrador
 */
public class Robot {
    
    private Punto posicion;

    public Robot() {
        this.posicion = new Punto(0,0);
    }

    public Robot(Punto posicion) {
        this.posicion = posicion;
    }

    public Punto getPosicion() {
        return this.posicion;
    }
    
    public double getX() {
        return this.posicion.getX();
    }
    
    public double getY() {
        return this.posicion.getY();
    }
    
    public void setX(double x) {
        this.posicion.setX(x);
    }
    
    public void setY(double y) {
        this.posicion.setY(y);
    }

    public void setPosicion(Punto posicion) {
        this.posicion = posicion;
    }
    
    public void moverArriba() {
        this.posicion.setY(this.getY() + 1);
    }
    
    public void moverDerecha() {
        this.posicion.setX(this.getX() + 1);
    }
    
    public void moverIzquierda() {
        this.posicion.setX(this.getX() - 1);
    }
    
    public void moverAbajo() {
        this.posicion.setY(this.getY() - 1);
    }

    @Override
    public String toString() {
        return "Robot{" + "posicion={ " + this.getX() + ", " + this.getY() + " }}";
    }
    
    
}
