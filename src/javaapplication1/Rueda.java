/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Administrador
 */
public class Rueda {
    
    private boolean inflada;

    public Rueda() {
        this.inflada = false;
    }

    public Rueda(boolean inflada) {
        this.inflada = inflada;
    }

    public boolean isInflada() {
        return this.inflada;
    }

    public void setInflada(boolean inflada) {
        this.inflada = inflada;
    }

    @Override
    public String toString() {
        return "Rueda{" + "inflada=" + inflada + '}';
    }
    
}
