/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Administrador
 */
public class Motor {
    
    private boolean prendido;

    public Motor() {
        this.prendido = false;
    }

    public Motor(boolean prendido) {
        this.prendido = prendido;
    }

    public boolean isPrendido() {
        return this.prendido;
    }

    public void setPrendido(boolean prendido) {
        this.prendido = prendido;
    }

    @Override
    public String toString() {
        return "Motor{" + "prendido=" + prendido + '}';
    }
    
    
}
