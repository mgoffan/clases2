/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Administrador
 */
public class Circulo {
    
    
    private Punto centro;
    private double radio;

    public Circulo() {
        this.centro = new Punto(0,0);
        this.radio = 0.0;
    }

    public Circulo(Punto centro, double radio) {
        this.centro = centro;
        this.radio = radio;
    }

    public Punto getCentro() {
        return centro;
    }

    public void setCentro(Punto centro) {
        this.centro = centro;
    }

    public double getRadio() {
        return this.radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
    
    public double area() {
        return this.radio * this.radio * Math.PI;
    }
    
    public double perimetro() {
        return Math.PI * this.radio * 2.0;
    }

    @Override
    public String toString() {
        return "Circulo{" + "centro={ " + centro.getX() + ", " + centro.getY() + " }, radio=" + radio + '}';
    }
    
    
}
