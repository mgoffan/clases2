/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Administrador
 */
public class Ventana {
    
    private boolean abierta;

    public Ventana() {
        this.abierta = false;
    }

    public Ventana(boolean abierta) {
        this.abierta = abierta;
    }

    public boolean isAbierta() {
        return this.abierta;
    }

    public void setAbierta(boolean abierta) {
        this.abierta = abierta;
    }

    @Override
    public String toString() {
        return "Ventana{" + "abierta=" + abierta + '}';
    }
    
    
}
