/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Administrador
 */
public class Puerta {
    
    private Ventana ventana;
    private boolean abierta;

    public Puerta() {
        this.ventana = new Ventana();
        this.abierta = false;
    }

    public Puerta(Ventana ventana, boolean abierta) {
        this.ventana = ventana;
        this.abierta = abierta;
    }

    public boolean isAbierta() {
        return this.abierta;
    }

    public void setAbierta(boolean abierta) {
        this.abierta = abierta;
    }

    public Ventana getVentana() {
        return this.ventana;
    }

    public void setVentana(Ventana ventana) {
        this.ventana = ventana;
    }

    @Override
    public String toString() {
        return "Puerta{" + "ventana=" + ((this.ventana.isAbierta()) ? "Abierta" : "Cerrada") + ", abierta=" + abierta + '}';
    }
    
    
}
